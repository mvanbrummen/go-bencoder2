package bencode

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBParser_parseInt(t *testing.T) {
	type fields struct {
		pos    uint
		source []byte
	}
	tests := []struct {
		name        string
		fields      fields
		want        int
		shouldPanic bool
	}{
		{"Should parse positive int successfully", fields{0, []byte("i123e")}, 123, false},
		{"Should parse 0 successfully", fields{0, []byte("i0e")}, 0, false},
		{"Should parse negative int successfully", fields{0, []byte("i-9e")}, -9, false},
		{"Should panic when no i at start", fields{0, []byte("1239e")}, -1, true},
		{"Should panic when no e at end", fields{0, []byte("i1239")}, -1, true},
		{"Should panic when no integers", fields{0, []byte("iaaae")}, -1, true},
		{"Should panic when no value", fields{0, []byte("ie")}, -1, true},
		{"Should panic when multiple negative signs present", fields{0, []byte("i--88e")}, -1, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &BParser{
				pos:    tt.fields.pos,
				source: tt.fields.source,
			}

			if tt.shouldPanic {
				assert.Panics(t, func() { p.parseInt() }, "Should panic")
			} else {
				if got := p.parseInt(); got != tt.want {
					t.Errorf("BParser.parseInt() = %v, want %v", got, tt.want)
				}
			}
		})
	}
}

func TestBParser_parseString(t *testing.T) {
	type fields struct {
		pos    uint
		source []byte
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{"Should parse valid string", fields{0, []byte("4:spam")}, "spam"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &BParser{
				pos:    tt.fields.pos,
				source: tt.fields.source,
			}
			if got := p.parseString(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BParser.parseString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBParser_parseList(t *testing.T) {
	type fields struct {
		pos    uint
		source []byte
	}
	tests := []struct {
		name   string
		fields fields
		want   BList
	}{
		{"Should parse valid list of strings", fields{0, []byte("l4:spam6:coffeee")}, BList{"spam", "coffee"}},
		{"Should parse valid list of ints", fields{0, []byte("li55ei123ei-345ei0ee")}, BList{55, 123, -345, 0}},
		{"Should parse valid list of lists", fields{0, []byte("ll4:spami123eel4:spam3:cutee")}, BList{
			BList{"spam", 123},
			BList{"spam", "cut"},
		},
		},
		{"Should parse valid list of dicts", fields{0, []byte("ld3:bar4:spam3:fooi42eee")}, BList{
			BDict{
				"bar": "spam",
				"foo": 42,
			},
		}},
		{"Should parse valid list of ints and strings", fields{0, []byte("li55e4:spami123ei-345ei0e6:coffeee")}, BList{55, "spam", 123, -345, 0, "coffee"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &BParser{
				pos:    tt.fields.pos,
				source: tt.fields.source,
			}
			assert.Equal(t, tt.want, p.parseList())
		})
	}
}

func TestBParser_parseDict(t *testing.T) {
	type fields struct {
		pos    uint
		source []byte
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{"Should parse valid dictionary", fields{0, []byte("d3:bar4:spam3:fooi42ee")}, 2},
		{"Should parse valid dictionary of dict", fields{0, []byte("d3:bar4:spam3:food3:bar4:spam3:fooi42eee")}, 2},
		{"Should parse valid dictionary of dict and lists", fields{0,
			[]byte("d3:bar4:spam3:food3:bar4:spam3:fooi42ee3:carll4:spami123eel4:spam3:cuteee")}, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &BParser{
				pos:    tt.fields.pos,
				source: tt.fields.source,
			}
			assert.Len(t, p.parseDict(), tt.want)
		})
	}
}
func Test_Parse(t *testing.T) {
	type args struct {
		source []byte
	}
	tests := []struct {
		name        string
		args        args
		shouldPanic bool
	}{
		{"Should parse string", args{[]byte("4:spam")}, true},
		{"Should parse int", args{[]byte("i33e")}, true},
		{"Should parse list", args{[]byte("li33ei33ee")}, true},
		{"Should parse dict", args{[]byte("d4:spamli33ei33ee3:cati456ee")}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.shouldPanic {
				assert.Panics(t, func() { Parse(tt.args.source) })
			} else {
				assert.NotNil(t, Parse(tt.args.source))
			}
		})
	}
}

func TestEncode(t *testing.T) {
	type args struct {
		bnode interface{}
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{"Should encode int", args{44}, []byte("i44e")},
		{"Should encode string", args{[]byte("spam")}, []byte("4:spam")},
		{"Should encode list", args{BList{32, 64}}, []byte("li32ei64ee")},
		{"Should encode dict", args{
			BDict{
				"spam": []byte("cat"),
				"port": 44,
			},
		}, []byte("d4:porti44e4:spam3:cate")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Encode(tt.args.bnode); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Encode() = %v, want %v", got, tt.want)
			}
		})
	}
}
