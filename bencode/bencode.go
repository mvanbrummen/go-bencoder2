package bencode

import (
	"bytes"
	"fmt"
	"sort"
	"strconv"
)

type (
	BInt    = int
	BString = string
	BList   = []interface{}
	BDict   map[string]interface{}
)

func (d *BDict) Contains(key string) bool {
	for _, k := range d.Keys() {
		if k == key {
			return true
		}
	}

	return false
}

func (d *BDict) Keys() []string {
	keys := make([]string, len(*d))

	for k, _ := range *d {
		keys = append(keys, k)
	}

	return keys
}

type BParser struct {
	pos    uint
	source []byte
}

func NewBParser(source []byte) *BParser {
	return &BParser{
		0,
		source,
	}
}

func Parse(source []byte) interface{} {
	p := NewBParser(source)

	switch p.nextChar() {
	case 'd':
		return p.parseDict()
	default:
		panic("BEncoded file expected to have dictionary at top level")
	}
}

func (p *BParser) nextChar() rune {
	if p.isEOF() {
		panic("Cannot read next char as end of file reached.")
	}

	return rune(p.source[p.pos])
}

func (p *BParser) consumeChar() rune {
	char := p.nextChar()

	p.pos++

	return char
}

func (p *BParser) consumeCharWhile(predicate func(rune) bool) string {
	var buf bytes.Buffer

	for !p.isEOF() && predicate(p.nextChar()) {
		buf.WriteRune(p.consumeChar())
	}

	return buf.String()
}

func (p *BParser) consumeCharFor(length int) string {
	var buf bytes.Buffer

	for i := 0; i < length; i++ {
		buf.WriteRune(p.consumeChar())
	}

	return buf.String()
}

func (p *BParser) isEOF() bool {
	return p.pos == uint(len(p.source))
}

func (p *BParser) parseInt() BInt {
	signMultiplier := 1

	if char := p.consumeChar(); char != 'i' {
		panic("Expecting an int to start with 'i'")
	}

	if char := p.nextChar(); char == '-' {
		p.consumeChar()
		signMultiplier = -1
	}

	intValue := p.consumeCharWhile(func(r rune) bool {
		return r >= '0' && r <= '9'
	})
	if char := p.consumeChar(); char != 'e' {
		panic("Expecting an int to end with 'e'")
	}
	i, err := strconv.Atoi(intValue)

	if err != nil {
		panic("Expecting int value to be a valid integer")
	}

	return BInt(i * signMultiplier)
}

func (p *BParser) parseString() BString {
	lengthString := p.consumeCharWhile(func(r rune) bool {
		return r != ':'
	})
	length, err := strconv.Atoi(lengthString)

	if err != nil {
		panic("String must begin with an integer defining length")
	}
	if char := p.consumeChar(); char != ':' {
		panic("Expecting an ':' before string value")
	}

	str := p.consumeCharFor(length)

	return BString(str)
}

func (p *BParser) parseDict() interface{} {
	if char := p.consumeChar(); char != 'd' {
		panic("Expecting a dict to start with 'd'")
	}

	dict := make(BDict)

	for !p.isEOF() {

		if p.nextChar() == 'e' {
			p.consumeChar()
			break
		}

		dictKey := string(p.parseString())

		switch p.nextChar() {
		case 'l':
			dict[dictKey] = p.parseList()
		case 'd':
			dict[dictKey] = p.parseDict()
		case 'i':
			dict[dictKey] = p.parseInt()
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			dict[dictKey] = p.parseString()
		default:
			panic("Unexpected char encountered during parsing list")
		}
	}

	return dict
}

func (p *BParser) parseList() interface{} {
	if char := p.consumeChar(); char != 'l' {
		panic("Expecting a list to start with 'l'")
	}

	list := make(BList, 0)

	for !p.isEOF() {
		if p.nextChar() == 'e' {
			p.consumeChar()
			break
		}

		switch p.nextChar() {
		case 'l':
			list = append(list, p.parseList())
		case 'd':
			list = append(list, p.parseDict())
		case 'i':
			list = append(list, p.parseInt())
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			list = append(list, p.parseString())
		default:
			panic("Unexpected char encountered during parsing list")
		}
	}

	return list
}

func Encode(bnode interface{}) []byte {
	var buf bytes.Buffer

	switch t := bnode.(type) {
	case BString:
		buf.WriteString(fmt.Sprintf("%d:%s", len(t), t))
	case []byte:
		buf.WriteString(fmt.Sprintf("%d:%s", len(t), t))
	case BInt:
		buf.WriteString(fmt.Sprintf("i%de", t))
	case BList:
		buf.WriteRune('l')
		for _, item := range t {
			buf.Write(Encode(item))
		}
		buf.WriteRune('e')
	case BDict:
		buf.WriteRune('d')

		keys := make([]string, 0, len(t))

		for key := range t {
			keys = append(keys, key)
		}

		sort.Strings(keys)

		for _, key := range keys {
			buf.Write(Encode([]byte(key)))
			buf.Write(Encode(t[key]))
		}
		buf.WriteRune('e')

	default:
		panic(fmt.Sprintf("Unknown type to encode+ %+v", t))
	}

	return buf.Bytes()
}
