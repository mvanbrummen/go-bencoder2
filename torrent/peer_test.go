package torrent

import (
	"reflect"
	"testing"
)

func TestEncodeHandshake(t *testing.T) {
	want := []byte{19, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111, 108, 0, 0, 0, 0, 0, 0, 0, 0, 115, 111, 109, 101, 104, 97, 115, 104, 115, 111, 109, 101, 104, 97, 115, 104, 104, 97, 115, 104, 105, 100, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48}

	type args struct {
		hash []byte
		id   string
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{"Should encode handshake", args{[]byte("somehashsomehashhash"), "id000000000000000000"}, want},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EncodeHandshake(tt.args.hash, tt.args.id); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EncodeHandshake() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecodeHandshake(t *testing.T) {
	argBytes := []byte{19, 66, 105, 116, 84, 111, 114, 114, 101, 110, 116, 32, 112, 114, 111, 116, 111, 99, 111, 108, 0, 0, 0, 0, 0, 0, 0, 0, 115, 111, 109, 101, 104, 97, 115, 104, 115, 111, 109, 101, 104, 97, 115, 104, 104, 97, 115, 104, 105, 100, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48}

	type args struct {
		bytes []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		want1   string
		wantErr bool
	}{
		{"Should decode handshake", args{argBytes},
			[]byte("somehashsomehashhash"),
			"id000000000000000000",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := DecodeHandshake(tt.args.bytes)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodeHandshake() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodeHandshake() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("DecodeHandshake() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
