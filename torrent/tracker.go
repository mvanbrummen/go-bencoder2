package torrent

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/mvanbrummen/go-bencoder2/bencode"
)

type TrackerState int

const (
	Started TrackerState = iota
	Paused
	Stopped
)

func (s TrackerState) String() string {
	return [...]string{"started", "paused", "stopped"}[s]
}

type Tracker struct {
	AnnounceURL string

	LastPeerRequest     time.Time
	PeerRequestInterval time.Duration
}

func (t *Tracker) Update(torrent *Torrent, trackerState TrackerState, peerId string, port int) {

	if trackerState == Started && t.LastPeerRequest.Add(t.PeerRequestInterval).After(time.Now()) {
		return
	}

	url := fmt.Sprintf("%s?info_hash=%s&peer_id=%s&port=%d&uploaded=%d&downloaded=%d&left=%d&event=%s&compact=1",
		t.AnnounceURL,
		torrent.UrlEncodedInfoHash(),
		peerId,
		port,
		torrent.Uploaded,
		torrent.Downloaded,
		torrent.Left,
		trackerState,
	)
	fmt.Println(url)

	resp, err := http.Get(url)
	if err != nil {
		panic("Could not make http announce request")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic("Http response could not be read")
	}

	fmt.Printf("\n\nbody: %s\n", body)

	dict := bencode.Parse(body).(bencode.BDict)

	var complete int
	if dict.Contains("complete") {
		complete = dict["complete"].(bencode.BInt)
	}

	var incomplete int
	if dict.Contains("incomplete") {
		incomplete = dict["incomplete"].(bencode.BInt)
	}

	var interval int
	if dict.Contains("interval") {
		interval = dict["interval"].(bencode.BInt)
		intervalDuration, err := time.ParseDuration(fmt.Sprintf("%dm", interval))

		if err != nil {
			panic("Could not parse interval")
		}

		t.PeerRequestInterval = intervalDuration
	}

	var minInterval int
	if dict.Contains("min interval") {
		minInterval = dict["min interval"].(bencode.BInt)
	}

	peers := make([]string, 0)
	peerInfo := dict["peers"].(bencode.BString)

	log.Printf("\nlength of peers %d\n", len(peerInfo))

	for i := 0; i < len(peerInfo)/6; i++ {
		offset := i * 6
		ip := fmt.Sprintf("%d.%d.%d.%d", peerInfo[offset], peerInfo[offset+1], peerInfo[offset+2], peerInfo[offset+3])
		port := peerInfo[offset+4]

		peers = append(peers, fmt.Sprintf("%s:%d", ip, port))
	}

	log.Printf(`
	peers: %d,
	leechers: %d,
	interval: %d,
	min interval: %d,
	peers: %+v
	`, complete, incomplete, interval, minInterval, peers)

}

type FileItem struct {
	Path   string
	Length int
}

func NewTracker(announceURL string) *Tracker {
	defaultPeerRequestInterval, _ := time.ParseDuration("30m")

	return &Tracker{
		AnnounceURL:         announceURL,
		PeerRequestInterval: defaultPeerRequestInterval,
	}
}

func NewFileItem(path string, length int) *FileItem {
	return &FileItem{path, length}
}
