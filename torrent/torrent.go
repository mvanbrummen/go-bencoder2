package torrent

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"net/url"

	"gitlab.com/mvanbrummen/go-bencoder2/bencode"
)

type Encoding int

const (
	UTF8 Encoding = iota
)

func ParseEncoding(str bencode.BString) Encoding {
	switch str {
	case "UTF-8":
		return UTF8
	default:
		return UTF8
	}
}

type Torrent struct {
	Name         string
	IsPrivate    bool
	DownloadDir  string
	Trackers     []*Tracker
	CreatedBy    string
	CreationDate int
	Encoding     Encoding
	Comment      string

	Files       []*FileItem
	PieceLength int
	PiecesHash  []byte

	Downloaded int
	Uploaded   int
	Left       int

	InfoHash [20]byte
}

func (t *Torrent) UrlEncodedInfoHash() string {
	return url.QueryEscape(fmt.Sprintf("%s", t.InfoHash))
}

func NewTorrent(dict bencode.BDict, downloadDir string) (*Torrent, error) {
	trackers := make([]*Tracker, 0)

	if dict.Contains("announce") {
		trackers = append(trackers, NewTracker(dict["announce"].(bencode.BString)))
	}

	if dict.Contains("announce-list") {
		for _, announce := range dict["announce-list"].(bencode.BList) {
			switch a := announce.(type) {
			case bencode.BList:
				for _, n := range a {
					trackers = append(trackers, NewTracker(n.(bencode.BString)))
				}
			case bencode.BString:
				trackers = append(trackers, NewTracker(a))
			}
		}
	}

	var comment string
	if dict.Contains("comment") {
		comment = dict["comment"].(bencode.BString)
	}

	var createdBy string
	if dict.Contains("created by") {
		createdBy = dict["created by"].(bencode.BString)
	}

	var creationDate int
	if dict.Contains("creation date") {
		creationDate = dict["creation date"].(bencode.BInt)
	}

	if !dict.Contains("info") {
		return nil, errors.New("Info dictionary must be present")
	}

	bencodedInfo := bencode.Encode(dict["info"])
	infoHash := sha1.Sum(bencodedInfo)

	infoDict := dict["info"].(bencode.BDict)

	files := make([]*FileItem, 0)

	if infoDict.Contains("name") && infoDict.Contains("length") {
		name := infoDict["name"].(bencode.BString)
		length := infoDict["length"].(bencode.BInt)

		files = append(files, NewFileItem(name, length))
	} else if dict.Contains("length") {
		// TODO multiple files
	} else {
		return nil, errors.New("No files present in torrent")
	}

	var pieceLength int
	if infoDict.Contains("piece length") {
		pieceLength = infoDict["piece length"].(bencode.BInt)
	}

	var isPrivate bool
	if infoDict.Contains("private") {
		privateInt := infoDict["private"].(bencode.BInt)
		isPrivate = privateInt == 1
	}

	var piecesHash []byte
	if infoDict.Contains("pieces") {
		piecesHash = []byte(infoDict["pieces"].(bencode.BString))
	}

	var encoding Encoding
	if infoDict.Contains("encoding") {
		encoding = ParseEncoding(infoDict["encoding"].(bencode.BString))
	}

	torrent := &Torrent{
		Trackers:     trackers,
		Files:        files,
		Comment:      comment,
		CreatedBy:    createdBy,
		CreationDate: creationDate,
		PieceLength:  pieceLength,
		IsPrivate:    isPrivate,
		DownloadDir:  downloadDir,
		PiecesHash:   piecesHash,
		Encoding:     encoding,
		InfoHash:     infoHash,
	}

	return torrent, nil
}

func (t *Torrent) UpdateTrackers(trackerState TrackerState, peerId string, port int) {
	for _, tracker := range t.Trackers {
		tracker.Update(t, trackerState, peerId, port)
	}
}
