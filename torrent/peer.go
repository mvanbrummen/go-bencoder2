package torrent

import (
	"bytes"
	"errors"
	"net"
)

type MessageType int

const (
	Unknown MessageType = iota
	Handshake
	KeepAlive
	Choke
	Unchoke
	Interested
	NotInterested
	Have
	Bitfield
	Request
	Piece
	Cancel
	Port
)

type Peer struct {
	Torrent Torrent

	IPEndpoint string

	IsDisconnected   bool
	IsHandshakeSent  bool
	IsPositionSent   bool
	IsChokeSent      bool
	IsInterestedSent bool

	IsHandshakeReceived  bool
	IsChokeReceived      bool
	IsInterestedReceived bool

	Uploaded   int
	Downloaded int
}

func NewPeer(torrent Torrent, address string) *Peer {
	return &Peer{
		Torrent:              torrent,
		IPEndpoint:           address,
		IsChokeSent:          true,
		IsInterestedSent:     false,
		IsChokeReceived:      true,
		IsInterestedReceived: false,
	}
}

func (p *Peer) Connect() {
	tcpAddr, err := net.ResolveTCPAddr("tcp", p.IPEndpoint)
	if err != nil {
		panic("Could not resolve IP Address")
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		panic("Could not dial tcp")
	}

	conn.Close()

}

func (p *Peer) SendHandshake(conn *net.TCPConn) {
	if p.IsHandshakeSent {
		return
	}

	p.IsHandshakeSent = true
}

func (p *Peer) Disconnect() {

}

func (p *Peer) sendBytes() {

}

func EncodeHandshake(hash []byte, id string) []byte {
	var buf bytes.Buffer

	buf.WriteByte(19)
	buf.WriteString("BitTorrent protocol")
	for i := 0; i < 8; i++ {
		buf.WriteByte(0)
	}
	for _, b := range hash {
		buf.WriteByte(b)
	}
	buf.WriteString(id)

	return buf.Bytes()
}

func DecodeHandshake(bytes []byte) ([]byte, string, error) {
	if len(bytes) != 68 || bytes[0] != 19 {
		return nil, "", errors.New("Handshake must be of length 68 with a first byte of 19")
	}

	if string(bytes[1:20]) != "BitTorrent protocol" {
		return nil, "", errors.New("Protocol must be BitTorrent protocol")
	}

	hash := bytes[28:48]
	id := bytes[48:]

	return hash, string(id), nil
}
