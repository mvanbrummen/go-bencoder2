package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/mvanbrummen/go-bencoder2/bencode"
	"gitlab.com/mvanbrummen/go-bencoder2/torrent"
)

var (
	file = flag.String("file", "ubuntu.torrent", "Path to a bencoded file")
)

func main() {
	flag.Parse()

	bencodedStr, err := ioutil.ReadFile(*file)
	if err != nil {
		log.Fatalf("Could not read file %s", *file)
	}

	dict := bencode.Parse(bencodedStr).(bencode.BDict)
	t, err := torrent.NewTorrent(dict, "/home/mvanbrummen/Downloads/")

	if err != nil {
		panic(fmt.Sprintf("Failed to create torrent from file %v", err))
	}

	fmt.Printf("\n\n%s", t.UrlEncodedInfoHash())

	t.UpdateTrackers(torrent.Started, "-AZ2200-6wfG2wk6wWLc", 57765)

}
